"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongodb = require("mongodb");
const routing_1 = require("./routing");
const url = require("url");
const mimeTypes = { json: 'application/json' };
const GET = 'GET', POST = 'POST', PUT = 'PUT', DELETE = 'DELETE';
/**
 * set the content of the response to application/json
 * and stringify data
 * @param response
 * @param data
 */
function respondWithJson(response, data) {
    response.setHeader('Content-Type', mimeTypes.json);
    response.end(JSON.stringify(data, null, '\t'));
}
function JSONParseBson(data) {
    return JSON.parse(data, (k, v) => mongodb.ObjectId.isValid(v) ? new mongodb.ObjectId(v) : v);
}
/* read all stream into a Buffer */
async function readAllStream(s, length) {
    return new Promise((resolve, reject) => {
        /** @todo deal with length since we are concatening buffers */
        let result = length ? Buffer.alloc(length) : Buffer.from('');
        s.on('data', data => {
            result = Buffer.concat([result, data]);
        });
        s.on('end', _ => {
            resolve(result);
        });
        s.on('error', err => {
            reject(err);
        });
    });
}
class Database {
    constructor(client) {
        this.client = client;
    }
    getClient() {
        if (this.client.isConnected() == false) {
            return this.client.connect();
        }
        return Promise.resolve(this.client);
    }
    listDatabases() {
        return this.getClient().then(c => c.db().admin().listDatabases());
    }
    findOneDocumentById(database, collection, _id) {
        return this.getClient().then(client => client.db(database).collection(collection).findOne(new mongodb.ObjectID(_id)));
    }
    updateDocument(database, collection, _id, data) {
        return this.getClient().then(client => client.db(database).collection(collection).update({ _id: new mongodb.ObjectId(_id) }, data));
    }
    delete(database, collection, _id) {
        return this.getClient().then(c => c.db(database).collection(collection).deleteOne({ _id: new mongodb.ObjectId(_id) }));
    }
    insertDocument(database, collection, json) {
        return this.getClient().then(c => c.db(database).collection(collection).insert(json));
    }
    findDocuments(database, collection, q, f, sk, l, s) {
        return this.getClient().then(c => c.db(database).collection(collection)
            .find(q, { fields: f, skip: sk, limit: l, sort: s }).toArray());
    }
    updateDocuments(database, collection, q, body, upsert, multi) {
        return this.getClient().then(c => c.db(database).collection(collection).update(q, body, { upsert, multi }));
    }
    listCollections(database) {
        return this.getClient().then(c => c.db(database).listCollections().toArray());
    }
}
class App {
    constructor(database, rootPath = "/api/1/") {
        this.database = database;
        this.rootPath = rootPath;
        this.router = new routing_1.default.Router();
        this.routeDefintions = [
            [this.rootPath + 'databases', this.databasesController, 'GET'],
            [this.rootPath + 'databases/:database/collections', this.listCollectionsController, 'GET'],
            [this.rootPath + 'databases/:database/collections/:collection', this.listDocumentsController, 'GET', 'POST', 'PUT'],
            [this.rootPath + 'databases/:database/collections/:collection/:_id', this.documentController, 'GET', 'POST', 'PUT', 'DELETE']
        ];
        this.requestListener = async (request, response) => {
            try {
                const now = (new Date).toUTCString();
                response.once('finish', _ => {
                    /** @see https://en.wikipedia.org/wiki/Common_Log_Format */
                    console.log("%s - - [%s] \"%s %s HTTP/%s\" %s", request.connection.remoteAddress, now, request.method, request.url, request.httpVersion, response.statusCode);
                });
                const parsedURL = url.parse(request.url, true);
                const resolved = this.router.resolve(parsedURL.pathname);
                if (!resolved) {
                    response.writeHead(400);
                    return response.end();
                }
                await resolved.route.fn.call(resolved.route.scope, request, response, { ...resolved.parameters, ...parsedURL.query });
            }
            catch (e) {
                console.error(e);
                if (response.headersSent)
                    return;
                response.statusCode = 500;
                respondWithJson(response, e.message);
            }
        };
        this.router.register('/', this.homeController, this);
        this.router.register('/favicon.ico', (_, res) => res.end());
        this.routeDefintions.forEach(([route, controller, ...methods]) => this.router.register(route, controller, this));
    }
    homeController(request, response) {
        respondWithJson(response, {
            routes: this.routeDefintions.map(([r, _, ...methods]) => [r, ...methods])
        });
    }
    async documentController(request, response, { database, collection, _id }) {
        const doc = await this.database.findOneDocumentById(database, collection, _id);
        if (!doc) {
            response.writeHead(404);
            return response.end();
        }
        switch (request.method) {
            case GET:
                return respondWithJson(response, doc);
            case PUT:
                const body = JSONParseBson((await readAllStream(request)).toString('utf8'));
                return respondWithJson(response, await this.database.updateDocument(database, collection, _id, body));
            case DELETE:
                return respondWithJson(response, await this.database.delete(database, collection, _id));
        }
    }
    async listCollectionsController(request, response, { database }) {
        respondWithJson(response, await this.database.listCollections(database));
    }
    async listDocumentsController(request, response, { m = "false", u = "false", s, f, l = 100, q, sk, database, collection }) {
        switch (request.method) {
            /** @todo implement delete behavior on PUT @see https://docs.mlab.com/data-api/#delete-documents */
            case PUT:
                const body = JSONParseBson((await readAllStream(request)).toString('utf8'));
                q = q ? JSONParseBson(q) : {};
                const multi = JSON.parse(m);
                const upsert = JSON.parse(u);
                return respondWithJson(response, await this.database.updateDocuments(database, collection, q, body, upsert, multi));
            case POST:
                const json = JSONParseBson((await readAllStream(request)).toString('utf8'));
                response.statusCode = 201;
                return respondWithJson(response, await this.database.insertDocument(database, collection, json));
            case GET:
                q = q ? JSONParseBson(q) : undefined;
                f = f ? JSON.parse(f) : undefined;
                return respondWithJson(response, await this.database.findDocuments(database, collection, q, f, sk, l, s));
            default:
                response.writeHead(405);
                response.end();
        }
    }
    async databasesController(request, response) {
        respondWithJson(response, await this.database.listDatabases());
    }
}
function createRequestListener(client, rootPath = "/api/1/") {
    const database = new Database(client);
    const app = new App(database, rootPath);
    return app.requestListener;
}
exports.default = { createRequestListener };
//# sourceMappingURL=index.js.map