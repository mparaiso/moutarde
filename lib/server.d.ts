/// <reference types="node" />
import * as http from 'http';
import * as mongodb from 'mongodb';
declare function createRequestListener(client: mongodb.MongoClient): (request: http.IncomingMessage, response: any) => Promise<void>;
declare const _default: {
    createRequestListener: typeof createRequestListener;
};
export default _default;
