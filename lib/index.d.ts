/// <reference types="node" />
import * as http from 'http';
import * as mongodb from 'mongodb';
declare function createRequestListener(client: mongodb.MongoClient, rootPath?: string): (request: http.IncomingMessage, response: http.ServerResponse) => Promise<void>;
declare const _default: {
    createRequestListener: typeof createRequestListener;
};
export default _default;
