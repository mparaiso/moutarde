import routing from './routing';
const mimeTypes = { json: 'application/json' };
function respondWithJson(response, data) {
    response.setHeader('Content-Type', mimeTypes.json);
    response.end(JSON.stringify(data, null, '\t'));
}
class App {
    constructor(client) {
        this.client = client;
        this.router = new routing.Router();
        this.requestListener = async (request, response) => {
        };
        this.router.register('/databases', this.databasesController, this);
    }
    async databasesController(request, response) {
        const client = await this.getClient();
        const databases = await client.db().admin().listDatabases();
        respondWithJson(response, databases);
    }
    getClient() {
        if (this.client.isConnected() == false) {
            return this.client.connect();
        }
        return Promise.resolve(this.client);
    }
}
function createRequestListener(client) {
    const app = new App(client);
    return app.requestListener;
}
export default { createRequestListener };
