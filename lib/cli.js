"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const index_1 = require("./index");
const http = require("http");
const mongodb = require("mongodb");
const ansiStyles = require("ansi-styles");
/* if main, start server */
if (!module.parent) {
    (async (_) => {
        const appId = 'MOUTARDE';
        const port = parseInt(process.env.MOUTARDE_PORT);
        const host = process.env.MOUTARDE_HOST;
        const client = new mongodb.MongoClient(process.env.MOUTARDE_MONGODB);
        const connection = await client.connect();
        const httpServer = http.createServer(index_1.default.createRequestListener(connection));
        httpServer.listen(port, host, _ => {
            console.log(`${ansiStyles.cyan.open}%s server listening on %s:%s with PID %s${ansiStyles.cyan.close}`, appId, host, port, process.pid);
        });
    })();
}
//# sourceMappingURL=cli.js.map