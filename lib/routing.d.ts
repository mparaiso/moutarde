export declare class Route {
    route: any;
    fn: Function;
    scope: any;
    routeParts: string[];
    constructor(route: any, fn: Function, scope: any);
    name(name: any): this;
    method(method: any): this;
    match(incomingRoute: any): {};
}
export declare class Router {
    routes: Route[];
    register(route: any, fn: any, scope?: any): Route;
    /**
     * returns a route and its parameter arguments
     * @param path the path to resolve
     */
    resolve(path: any): {
        route: Route;
        parameters: {};
    };
    /**
     * call a route handler given its scope and its route parameters if path resolved
     * @param path the path to resolve
     */
    execute(path: any): any;
}
declare const _default: {
    Router: typeof Router;
};
export default _default;
