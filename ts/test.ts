// @ts-check

import * as mongodb from 'mongodb'
import * as assert from 'assert'
import moutarde from './index.js'
/**
 * 
 * @param {{test:Function,name:string}[]} tests 
 */
const suite = async (tests) => {
    let error;
    for (const test of tests) {
        console.log(test.name)
        try {
            await test.test();
        } catch (e) {
            error = true;
            console.error(e)
        }
    }
    if (error) {
        process.exit(1)
    }
}

const tests = [
    {
        name: 'initial', test: _ => {
            assert.ok(true)
        }
    }, {
        name: 'moutarde', test: async _ => {
            const connection = new mongodb.MongoClient(process.env.MONGODB_MOUTARGE)
            const client = await connection.connect()
        }
    }
]

suite(tests)
