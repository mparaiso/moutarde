// @ts-check
// @see https://www.codeproject.com/Articles/495826/Roll-your-own-A-JavaScript-router
// @version 0.1

let parseURL;

export class Route {

    routeParts: string[] = this.route.split("/");

    constructor(public route, public fn: Function, public scope) { }

    name(name) {
        this.name = name
        return this
    }
    method(method) {
        this.method = method
        return this
    }
    match(incomingRoute) {
        const incomingRouteParts = incomingRoute.split("/");
        const routeArguments = {};
        for (let i = 0, j = incomingRouteParts.length; i < j; i++) {
            const incomingRoutePart = incomingRouteParts[i];
            const routePart = this.routeParts[i];
            if (typeof routePart === 'undefined') return
            if (routePart.startsWith(":"))
                routeArguments[routePart] = routeArguments[routePart.slice(1)] = decodeURIComponent(incomingRoutePart);
            else if (routePart.startsWith('*')) {
                routeArguments[routePart] = routeArguments[routePart.slice(1)] = decodeURIComponent(incomingRouteParts.slice(i).join('/'));
                return routeArguments;
            }
            else if (incomingRoutePart != routePart)
                return;
        }
        return routeArguments;
    }
}
export class Router {

    routes: Route[] = [];
    register(route, fn, scope?) {
        const r = new Route(route, fn, scope)
        this.routes.push(r);
        return r
    }
    /**
     * returns a route and its parameter arguments
     * @param path the path to resolve
     */
    resolve(path) {
        for (const r of this.routes) {
            const routeArguments = r.match(path);
            if (routeArguments)
                return { route: r, parameters: routeArguments };
        }
    }
    /**
     * call a route handler given its scope and its route parameters if path resolved
     * @param path the path to resolve
     */
    execute(path) {
        const resolved = this.resolve(path);
        if (resolved) {
            return resolved.route.fn.call(resolved.route.scope, resolved.parameters);
        }
    }
}

export default { Router }