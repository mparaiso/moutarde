
import server from './index'
import * as http from 'http'
import * as mongodb from 'mongodb'
import * as ansiStyles from 'ansi-styles'

/* if main, start server */
if (!module.parent) {
    (async _ => {
        const appId = 'MOUTARDE'
        const port: number = parseInt(process.env.MOUTARDE_PORT)
        const host = process.env.MOUTARDE_HOST
        const client = new mongodb.MongoClient(process.env.MOUTARDE_MONGODB)
        const connection = await client.connect()
        const httpServer = http.createServer(server.createRequestListener(connection))
        httpServer.listen(port, host, _ => {
            console.log(`${ansiStyles.cyan.open}%s server listening on %s:%s with PID %s${ansiStyles.cyan.close}`, appId, host, port, process.pid)
        })
    })()
}

