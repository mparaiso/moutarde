###Moutarde is a drop-in replacement for mlab REST API

author: mparaiso <mparaiso at online.fr>

license: GNU GPLv3

Moutarde was designed in case mlab abandons its REST API, allowing developers to continue 
using their app build upon mlab REST API with minimal changes. mlad is written in Javascript.

####requirements:

node.js version 9.2.0
mongodb version 3.6.8


references:

http://2ality.com/2018/04/async-iter-nodejs.html 